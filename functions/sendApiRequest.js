import { AsyncStorage, Alert } from "react-native";
import { CommonActions } from '@react-navigation/native';

export default function sendApiRequest(endpoint, data, callback, token, navigation) {
    var xhr = new XMLHttpRequest();

    xhr.open("POST", 'https://petgoo.framework360.it/m/api' + endpoint, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.setRequestHeader('X-Fw360-Key', '9XbHPsALrgEIxhMd8N1azYtJBpSyFkv2uVQCTU5n');
    xhr.setRequestHeader('X-Fw360-UserToken', token);
    xhr.send(JSON.stringify(data));
    xhr.onload = function () {
        var data = JSON.parse(this.responseText);
        if (typeof (data.error) != 'undefined' && data.error === 'user_token_expired') {
            AsyncStorage.clear();
            Alert.alert('La tua sessione è scaduta');
            navigation()
        }
        else {
            callback(JSON.parse(this.responseText));
        }
    }
}

