import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    AsyncStorage,
    Image,
    ScrollView,
    TouchableOpacity,
    ActivityIndicator,
} from "react-native";
import sendApiRequest from '../functions/sendApiRequest';


class SingleOrder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            order: [],
            cart: [],
            token:null,
        };
    }

    componentDidMount = async () => {
        const that = this;
        const id = await this.props.route.params.id;
        const seller = await AsyncStorage.getItem("account");
        const accountFormatted = await JSON.parse(seller);
        const {navigation} = this.props;
        
        await this.setState({
          token:accountFormatted.data.access_token
        });
        sendApiRequest('/orders/get/', {
            id: id
        }, function (data) {
            console.log(data.data.billing_data)
            that.setState({
                order: data,
                cart: data.data.cart.products
            })
        },
            that.state.token,
            function(){
                that.props.navigation.dispatch(
                  CommonActions.reset({
                      index: 0,
                      routes: [
                          { name: 'Login' },
                      ],
                  })
              );
              }
        );
    }

    render() {
        return (
            <ScrollView style={{ flex: 1 }} contentContainerStyle={{ width: '95%', marginLeft: 'auto', marginTop: '5%' }}>
                {this.state.order.data ?
                    <View >
                        <View>
                            <Text style={{ fontWeight: '700', fontSize: 25 }}>Carrello:</Text>
                            {this.state.cart.map((item, index) => (
                                <View
                                    key = {index}
                                    style={{
                                        borderBottomWidth: 1,
                                        borderBottomColor: "#EAE9E9",
                                        width: "100%",
                                        padding: 10,
                                        flexDirection: "row",
                                    }}
                                >
                                    <View>
                                        <Image
                                            style={{ height: 100, width: 60, resizeMode: "contain" }}
                                            source={{
                                                uri: item.preview,
                                            }}
                                        />
                                    </View>
                                    <View style={{ justifyContent: "space-around", marginLeft: "2%" }}>
                                        <Text style={{ fontWeight: "600", fontSize: 18 }}>
                                            {item.title.substring(0, 25)}...
                                        </Text>
                                        <Text style={{ fontWeight: "500", fontSize: 18 }}>
                                            x {item.quantity}
                                        </Text>
                                        <Text style={{ fontWeight: "600" }}>
                                            {item.prezzo.no_tax.toFixed(2)} €
                                        </Text>
                                    </View>
                                </View>
                            ))}


                        </View>
                        <View style={styles.clientBox}>
                            <Text style={{ fontWeight: '700', fontSize: 25 }}>Cliente:</Text>
                            <View style={{ marginTop: '5%' }}>
                                <View style={{ borderBottomWidth: 1, borderColor: 'grey', width: '95%' }}>
                                    <Text style={{ fontWeight: '700', fontSize: 18 }}>Ragione Sociale:</Text>
                                    <Text style={{ fontWeight: '400', fontSize: 18, marginTop: 10, marginBottom: 10 }}>{this.state.order.data.billing_data.ragione_sociale}</Text>
                                </View>
                                <View style={{ borderBottomWidth: 1, borderColor: 'grey', width: '95%', marginTop: 10 }}>
                                    <Text style={{ fontWeight: '700', fontSize: 18 }}>Indirizzo:</Text>
                                    <Text style={{ fontWeight: '400', fontSize: 18, marginTop: 10, marginBottom: 10 }}>{this.state.order.data.billing_data.indirizzo}</Text>
                                </View>
                                <View style={{ borderBottomWidth: 1, borderColor: 'grey', width: '95%', marginTop: 10 }}>
                                    <Text style={{ fontWeight: '700', fontSize: 18 }}>Città:</Text>
                                    <Text style={{ fontWeight: '400', fontSize: 18, marginTop: 10, marginBottom: 10 }}>{this.state.order.data.billing_data.citta}</Text>
                                </View>
                                <View style={{ borderBottomWidth: 1, borderColor: 'grey', width: '95%', marginTop: 10 }}>
                                    <Text style={{ fontWeight: '700', fontSize: 18 }}>Cap:</Text>
                                    <Text style={{ fontWeight: '400', fontSize: 18, marginTop: 10, marginBottom: 10 }}>{this.state.order.data.billing_data.cap}</Text>
                                </View>
                                <View style={{ borderBottomWidth: 1, borderColor: 'grey', width: '95%', marginTop: 10 }}>
                                    <Text style={{ fontWeight: '700', fontSize: 18 }}>Partita iva:</Text>
                                    <Text style={{ fontWeight: '400', fontSize: 18, marginTop: 10, marginBottom: 10 }}>{this.state.order.data.billing_data.piva}</Text>
                                </View>
                                <View style={{ borderBottomWidth: 1, borderColor: 'grey', width: '95%', marginTop: 10 }}>
                                    <Text style={{ fontWeight: '700', fontSize: 18 }}>Cellulare:</Text>
                                    <Text style={{ fontWeight: '400', fontSize: 18, marginTop: 10, marginBottom: 10 }}>{this.state.order.data.billing_data.cellulare}</Text>
                                </View>
                                <View style={{ borderBottomWidth: 1, borderColor: 'grey', width: '95%', marginTop: 10, }}>
                                    <Text style={{ fontWeight: '700', fontSize: 18 }}>Sdi:</Text>
                                    <Text style={{ fontWeight: '400', fontSize: 18, marginTop: 10,  }}>{this.state.order.data.billing_data.sdi}</Text>
                                </View>
                                <View style={{ borderBottomWidth: 1, borderColor: 'grey', width: '95%', marginTop: 10, marginBottom: '10%' }}>
                                    <Text style={{ fontWeight: '700', fontSize: 18 }}>Note:</Text>
                                    <Text style={{ fontWeight: '400', fontSize: 18, marginTop: 10, marginBottom: 10 }}>{this.state.order.data.note}</Text>
                                </View>


                            </View>
                        </View>
                    </View>
                    :
                    <ActivityIndicator></ActivityIndicator>
                }

            </ScrollView>
        );
    }
}
export default SingleOrder;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    clientBox: {
        marginTop: '5%'
    },

    field: {
        borderBottomWidth: 1,
        borderBottomColor: '#EAE9E9',
        width: '95%',
        padding: 10,
        marginBottom: 10,
        marginTop: 10,
        alignSelf: 'center'
    }

});