import React, { Component } from "react";
import {
  View,
  StyleSheet,
  FlatList,
  Text,
  TextInput,
  Dimensions,
  SafeAreaView,
  KeyboardAvoidingView,
  TouchableOpacity,
  AsyncStorage,
  Alert,
  Modal,
  ActivityIndicator,
} from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { Ionicons } from "@expo/vector-icons";
import sendApiRequest from "../functions/sendApiRequest";
import RNPickerSelect from 'react-native-picker-select';
import { CartContext } from "./../Context/CartContext";
import { cos } from "react-native-reanimated";
import { CommonActions } from '@react-navigation/native';


const featuredWidth = Dimensions.get("window").width - (Dimensions.get("window").width / 100) * 10;


function mapCart(products) {
  const cart = [];
  products.forEach((product, index) => {
    const item = {
      type: "product",
      id: product.id,
      quantity: product.quantity,
      clientQuery: null,
      customInfo: {
        discounts: [product.discount1, product.discount2, product.discount3],
      },
    };
    cart.push(item);
  });
  return cart;
}

function mapBillingData(customer) {
  const item = {
    cellulare: customer.cell,
    telefono: customer.tel,
    indirizzo: customer.indirizzo_fatturazione,
    indirizzo_2: "",
    stato: "3175395",
    citta: customer.provincia_fatturazione,
    comune: customer.città_fatturazione,
    cap: customer.cap_fatturazione,
    ragione_sociale: customer.ragione_sociale,
    piva: customer.p_iva,
    cf: "",
    sdi: customer.sdi,

  };
  return item;
}

function mapShippingData(customer) {
  const item = {
    indirizzo: customer.indirizzo,
    indirizzo_2: customer.luogo_consegna,
    stato: "3175395",
    citta: customer.citta,
    comune: customer.comune,
    cap: customer.cap_città,
    salva_info: "on",
    spedizione_diversa_fatturazione: "0",
    usa_fatturazione: 1,
  };
  return item;
}

class CheckoutScreen extends Component {
  constructor() {
    super();
    this.state = {
      ragione_sociale: "",
      indirizzo: "",
      indirizzo_fatturazione: '',
      cap_città: 0,
      p_iva: "",
      sdi: "",
      tel: 0,
      cell: 0,
      email: "",
      luogo_consegna: "",
      pagamento: "",
      metodi: [],
      citta: "",
      banca: "",
      comune: "",
      iban: "",
      token: null,
      note_cliente: "",
      modalVisible: false,
      clients: [],
      query: "",
      selectedMethod: '',
      selectedDelivery: '',
      provincia_fatturazione: '',
      cap_fatturazione: '',
      città_fatturazione: '',
      loading: false,
      delivery: [
        { label: 'Anticipato', value: 'Anticipato' },
        { label: 'Alla consegna', value: 'Alla consegna' },
        { label: 'A 30gg f.m', value: 'A 30gg f.m' },
        { label: 'A 60gg f.m', value: 'A 60gg f.m' }

      ]
    };
  }

  static contextType = CartContext;



  openModal = () => {
    this.setState({
      modalVisible: true,
    });
    this.searchClients();
  };

  autoComplete = async (item) => {
    const client = await item;
    this.setState({
      //name: client.nome,
      //surname: client.cognome,
      ragione_sociale: client.dati_fatturazione.ragione_sociale,
      indirizzo: client.indirizzo,
      cap_città: client.cap,
      comune: client.comune,
      luogo_consegna: client.indirizzo_2,
      provincia_fatturazione: client.dati_fatturazione.citta,
      città_fatturazione: client.dati_fatturazione.comune,
      cap_fatturazione: client.dati_fatturazione.cap,
      p_iva: client.dati_fatturazione.piva,
      sdi: client.dati_fatturazione.sdi,
      tel: client.telefono_casa,
      cell: client.telefono_cellulare,
      citta: client.citta,
      email: client.email,
      modalVisible: false,

    });
  };

  searchClients = () => {
    const that = this;

    sendApiRequest(
      "/customers/search/",
      {
        query: this.state.query,
      },
      function (data) {
        that.setState({ clients: data.data });
        console.log(data)
      },
      this.state.token,
      function () {
        navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [
              { name: 'Login' },
            ],
          })
        );
      }
    );
  };

  getShippingData = () => {
    const data = this.state;
    const shippingData = mapShippingData(data);

    return shippingData;
  };

  getCart = async () => {
    const fromAsync = await AsyncStorage.getItem("products");
    const products = await JSON.parse(fromAsync);
    const cart = await mapCart(products);

    return cart;
  };

  getBillingData = () => {
    const data = this.state;
    const customer = mapBillingData(data);

    return customer;
  };

  redirectToSuccess = () => {
    this.props.navigation.navigate("ThankYou");
  };

  getPaymentsOptions = async () => {
    const that = this;
    const { navigation } = this.props;
    sendApiRequest(
      '/payments/list/',
      {

      },
      async (data) => {
        const custom = await data.data.custom;
        await custom.forEach(metodo => {
          metodo.label = metodo.name,
            metodo.value = metodo.name
        });


        const web = await Object.values(data.data.web);
        await web.forEach(metodo => {
          metodo.label = metodo.name,
            metodo.value = metodo.id
        });

        const methods = await custom.concat(web);
        await that.setState({
          metodi: methods
        });
      },

      this.state.token,
      function () {
        navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [
              { name: 'Login' },
            ],
          })
        );
      }
    )
  }

  componentDidMount = async () => {
    const account = await AsyncStorage.getItem('account')
    const parsed = await JSON.parse(account)
    await this.setState({
      token: parsed.data.access_token
    });
    this.getPaymentsOptions();

  }

  createOrder = async () => {
    const cart = await this.getCart();
    const billing_data = await this.getBillingData();
    const shippingData = await this.getShippingData();

    const that = this;
    if (billing_data.ragione_sociale == '' || billing_data.indirizzo == '' || billing_data.comune == '' || billing_data.cap == '' || billing_data.indirizzo == '' || billing_data.citta == '' || billing_data.piva == '' || billing_data.sdi == '' || billing_data.email == '' || billing_data.telefono == '' || this.state.selectedMethod == '') {
      Alert.alert('Compila tutti i campi prima di inviare un ordine ');

    }
    else if (this.state.selectedMethod == 'Contanti' && this.state.selectedDelivery == '') {
      Alert.alert('Inserisci il metodo di pagamento');

    }
    else if (this.state.selectedMethod == 'Ri.Ba' && this.state.iban == '' || this.state.selectedMethod == 'Ri.Ba' && this.state.banca == '') {
      Alert.alert('Compila i dati bancari ');

    }

    else {
      this.setState({ loading: true })

      await sendApiRequest("/orders/create/",
        {
          user_email: this.state.email,
          cart: cart,
          billing_data: billing_data,
          shipping_data: shippingData,
          note: this.state.note_cliente + ' ' + this.state.banca + ' ' + this.state.iban,
          labels: [this.state.selectedDelivery, this.state.selectedMethod],
          payment_id: this.state.selectedMethod
        },
        async () => {
          this.setState({ loading: false })
        },
        this.state.token,
        function () { navigation.dispatch(CommonActions.reset({ index: 0, routes: [{ name: 'Login' },], })); });
    }

  };

  render() {
    const placeholder = {
      label: 'Pagamento',
      value: null,
      color: 'black',
    };

    return (
      <View style={{ flex: 1 }}>
        <KeyboardAvoidingView
          behavior={Platform.OS == "ios" ? "padding" : "height"}
        >
          <ScrollView
            contentContainerStyle={{
              justifyContent: "center",
              alignItems: "center",
              marginTop: "5%",
            }}
          >
            <View style={{ width: "90%" }}>
              <TouchableOpacity
                onPress={() => this.openModal()}
                style={{
                  width: "100%",
                  alignSelf: "center",
                  borderWidth: 2,
                  borderColor: "#EE0750",
                  padding: 10,
                  justifyContent: "center",
                  alignItems: "center",
                  borderRadius: 10,
                  marginBottom: 20,
                }}
              >
                <Text
                  style={{ color: "#EE0750", fontWeight: "700", fontSize: 18 }}
                >
                  Ricerca un cliente
                </Text>
              </TouchableOpacity>

              <Text
                style={{
                  fontWeight: "600",
                  fontSize: 22,
                  marginBottom: 10,
                  marginTop: 10,
                }}
              >
                Dati di fatturazione:
              </Text>

              <TextInput
                value={this.state.ragione_sociale}
                onChangeText={(ragione_sociale) =>
                  this.setState({ ragione_sociale: ragione_sociale })
                }
                style={styles.input}
                placeholder={"Ragione Sociale (obbligatorio)"}
              ></TextInput>
              <TextInput
                value={this.state.indirizzo_fatturazione}
                onChangeText={(indirizzo_fatturazione) =>
                  this.setState({ indirizzo_fatturazione: indirizzo_fatturazione })
                }
                style={styles.input}
                placeholder={"Via (obbligatorio)"}
              ></TextInput>
              <TextInput
                value={this.state.città_fatturazione}
                onChangeText={(città_fatturazione) =>
                  this.setState({ città_fatturazione: città_fatturazione })
                }
                style={styles.input}
                placeholder={"Comune (obbligatorio)"}
              ></TextInput>
              <TextInput
                value={this.state.cap_fatturazione}
                onChangeText={(cap_fatturazione) =>
                  this.setState({ cap_fatturazione: cap_fatturazione })
                }
                style={styles.input}
                placeholder={"Cap (obbligatorio)"}
              ></TextInput>
              <TextInput
                value={this.state.provincia_fatturazione}
                onChangeText={(provincia_fatturazione) =>
                  this.setState({ provincia_fatturazione: provincia_fatturazione })
                }
                style={styles.input}
                placeholder={"Provincia (obbligatorio)"}
              ></TextInput>
              <TextInput
                value={this.state.p_iva}
                onChangeText={(p_iva) => this.setState({ p_iva: p_iva })}
                style={styles.input}
                placeholder={"P.iva (obbligatorio)"}
              ></TextInput>
              <TextInput
                value={this.state.sdi}
                onChangeText={(sdi) => this.setState({ sdi: sdi })}
                style={styles.input}
                placeholder={"Sdi (obbligatorio)"}
              ></TextInput>

              <Text
                style={{
                  fontWeight: "600",
                  fontSize: 22,
                  marginBottom: 10,
                  marginTop: 10,
                }}
              >
                Luogo di consegna diverso da ragione sociale:
              </Text>
              <TextInput
                value={this.state.indirizzo}
                onChangeText={(indirizzo) =>
                  this.setState({ indirizzo: indirizzo })
                }
                style={styles.input}
                placeholder={"Via"}
              ></TextInput>
              <TextInput
                value={this.state.comune}
                onChangeText={(comune) =>
                  this.setState({ comune: comune })
                }
                style={styles.input}
                placeholder={"Comune"}
              ></TextInput>

              <TextInput
                value={this.state.cap_città}
                onChangeText={(cap_città) =>
                  this.setState({ cap_città: cap_città })
                }
                style={styles.input}
                placeholder={"Cap"}
              ></TextInput>

              <Text
                style={{
                  fontWeight: "600",
                  fontSize: 22,
                  marginBottom: 10,
                  marginTop: 10,
                }}
              >
                Contatti cliente:
              </Text>
              <TextInput
                onEndEditing={() => this.autoComplete()}
                value={this.state.email}
                autoCapitalize={"none"}
                onChangeText={(email) => this.setState({ email: email })}
                style={styles.input}
                placeholder={"Email (obbligatorio)"}
              ></TextInput>
              <TextInput
                value={this.state.tel}
                onChangeText={(tel) => this.setState({ tel: tel })}
                style={styles.input}
                placeholder={"Telefono (obbligatorio)"}
              ></TextInput>
              <TextInput
                value={this.state.cell}
                onChangeText={(cell) => this.setState({ cell: cell })}
                style={styles.input}
                placeholder={"Cellulare"}
              ></TextInput>
              <Text
                style={{
                  fontWeight: "600",
                  fontSize: 22,
                  marginBottom: 10,
                  marginTop: 10,
                }}
              >
                Seleziona tipo di pagamento:
              </Text>
              <RNPickerSelect
                placeholder={{ label: 'Seleziona tipo di pagamento (obbligatorio)', value: '' }}
                style={pickerSelectStyles}
                onValueChange={(value) => this.setState({ selectedMethod: value })}
                onClose={() => console.log(this.state.selectedMethod)}
                items={this.state.metodi}
                value={this.state.selectedMethod}
              />
              <Text
                style={{
                  fontWeight: "600",
                  fontSize: 22,
                  marginBottom: 10,
                  marginTop: 10,
                }}
              >
                Seleziona metodo di pagamento:
              </Text>
              <RNPickerSelect
                placeholder={{ label: 'Seleziona metodo di pagamento', value: '' }}
                style={pickerSelectStyles}
                onValueChange={(value) => this.setState({ selectedDelivery: value })}
                onClose={() => console.log(this.state.selectedDelivery)}
                items={this.state.delivery}
                value={this.state.selectedDelivery}
              />
              <Text
                style={{
                  fontWeight: "600",
                  fontSize: 22,
                  marginBottom: 10,
                  marginTop: 10,
                }}
              >
                Dati bancari:
              </Text>
              <TextInput
                value={this.state.banca}
                onChangeText={(banca) => this.setState({ banca: banca })}
                style={styles.input}
                placeholder={"Banca"}
              ></TextInput>


              <TextInput
                value={this.state.iban}
                onChangeText={(iban) => this.setState({ iban: iban })}
                style={styles.input}
                placeholder={"Iban"}
              ></TextInput>

              <TextInput
                multiline={true}
                numberOfLines={4}
                value={this.state.note_cliente}
                onChangeText={(note_cliente) =>
                  this.setState({ note_cliente: note_cliente })
                }
                style={[styles.input, { marginBottom: 40, height: 100 }]}
                placeholder={"Note cliente"}
              ></TextInput>
            </View>
            {this.state.loading === false ?
              <TouchableOpacity
                onPress={() => this.createOrder()}
                style={styles.btn}
              >
                <Text style={styles.btnText}>Completa ordine</Text>
              </TouchableOpacity>
              :
              <TouchableOpacity
                style={styles.btn}
              >
                <ActivityIndicator color={'white'}></ActivityIndicator>
              </TouchableOpacity>
            }

          </ScrollView>
        </KeyboardAvoidingView>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
        >
          <View
            style={{
              backgroundColor: "white",
              width: "100%",
              height: "100%",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <SafeAreaView style={{ width: Dimensions.get("window").width }}>
              <View
                style={{
                  flexDirection: "row",
                  width: "90%",
                  alignItems: "center",
                  justifyContent: "space-between",
                  marginBottom: 20,
                  alignSelf: "center",
                }}
              >
                <TouchableOpacity
                  style={{ width: "10%" }}
                  onPress={() => this.setState({ modalVisible: false })}
                >
                  <Ionicons name="ios-arrow-back" size={34} color="#EE0750" />
                </TouchableOpacity>
                <View
                  style={{
                    width: "80%",
                    justifyContent: "center",
                    alignItems: "center",
                    marginRight: "auto",
                  }}
                >
                  <Text style={{ fontWeight: "bold" }}>RICERCA CLIENTE</Text>
                </View>
              </View>
              <TextInput
                onEndEditing={() => this.searchClients()}
                value={this.state.query}
                onChangeText={(query) => this.setState({ query: query })}
                style={{
                  backgroundColor: "#EAE9E9",
                  padding: 15,
                  width: "90%",
                  alignSelf: "center",
                }}
                placeholder={"Cerca..."}
              ></TextInput>
            </SafeAreaView>


            <FlatList
              contentContainerStyle={{
                width: featuredWidth,
                alignItems: "flex-start",
                marginTop: "10%",
              }}
              data={this.state.clients}
              showsVerticalScrollIndicator={false}
              horizontal={false}
              numColumns={1}
              keyExtractor={(x, i) => i.toString()}
              renderItem={({ item }) => (
                <TouchableOpacity
                  onPress={() => this.autoComplete(item)}
                  style={{
                    padding: 10,
                    borderBottomColor: "#EAE9E9",
                    borderBottomWidth: 1,
                    width: "100%",
                  }}
                >
                  <Text style={{ fontWeight: "bold", fontSize: 20 }}>
                    {item.dati_fatturazione.ragione_sociale}
                  </Text>
                  <View style={{ flexDirection: "row" }}>
                    <View
                      style={{
                        backgroundColor: "#EE0750",
                        padding: 5,
                        borderRadius: 5,
                        width: "22%",
                        marginTop: 10,
                      }}
                    >
                      <Text style={{ color: "white", textAlign: "center" }}>
                        P.iva
                        </Text>
                    </View>
                    <View
                      style={{
                        padding: 5,
                        borderRadius: 5,
                        width: "68%",
                        marginTop: 10,
                        marginLeft: 10,
                      }}
                    >
                      <Text
                        style={{
                          fontWeight: "500",
                          fontSize: 18,
                          color: "grey",
                        }}
                      >
                        {item.dati_fatturazione.piva}
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>
              )}
            />

          </View>
        </Modal>
      </View>
    );
  }
}
export default CheckoutScreen;

const styles = StyleSheet.create({
  input: {
    color: "black",
    width: "100%",
    borderWidth: 1,
    borderColor: "black",
    marginBottom: 20,
    padding: 10,
  },
  btnText: {
    color: "white",
  },
  btn: {
    backgroundColor: "#EE0750",
    justifyContent: "center",
    alignItems: "center",
    padding: 15,
    borderRadius: 10,
    marginBottom: "50%",
    width: "90%",
    alignSelf: "center",
  },
});
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    color: "black",
    width: "100%",
    borderWidth: 1,
    borderColor: "black",
    marginBottom: 20,
    padding: 10,
  },
  inputAndroid: {
    color: "black",
    width: "100%",
    borderWidth: 1,
    borderColor: "black",
    marginBottom: 20,
    padding: 10,
  },
});
