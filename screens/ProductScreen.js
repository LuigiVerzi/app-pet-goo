import React from "react";
import { Text, View, StyleSheet, TouchableOpacity, ActivityIndicator, Image, Dimensions, TextInput, AsyncStorage, Alert, } from "react-native";
import { CartContext } from "./../Context/CartContext";
import sendApiRequest from "../functions/sendApiRequest";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import HTML from 'react-native-render-html';
import { CommonActions } from '@react-navigation/native';

class ProductScreen extends React.Component {

  state = {
    product: [],
    display: "flex",
    price: null,
    sconto1: null,
    sconto2: null,
    sconto3: null,
    total: null,
    quantity: 0,
    count: 0,
    token: null
  };
  static contextType = CartContext;

  getProductFromApi = async (navigation) => {
    const that = this;
    const productId = this.props.route.params.id;
    const account = await AsyncStorage.getItem('account')
    const parsed = await JSON.parse(account)
    await this.setState({
      token:parsed.data.access_token
    });

    sendApiRequest(
      "/ecommerce/products/get/",
      {
        id: productId,
      },
      function (data) {
        const tmp = Object.values(data.data.formattedAttributes.attribute)[0].value
        console.log(data)
        that.setState({
          product: [data],
          num: Object.values(tmp)
        });
      },
     this.state.token,
      function(){
        navigation.dispatch(
          CommonActions.reset({
              index: 0,
              routes: [
                  { name: 'Login' },
              ],
          })
      );
      }
    );
  };

  getCount = async () => {
    const count = await AsyncStorage.getItem("cartCount");
    this.setState({
      count: count,
    });
  };

  componentDidMount() {
    const { navigation } = this.props;
    this.getProductFromApi(navigation);
  }


  getTotal = (fetchedPrice) => {
    let price = fetchedPrice;
    let sconto1 = this.state.sconto1;
    let sconto2 = this.state.sconto2;
    let sconto3 = this.state.sconto3;
    let quantity = this.state.quantity
    let sconto;
    let total;

    if (sconto1) {
      sconto = (price * sconto1) / 100;
      total = price - sconto;

      if (sconto2) {
        sconto = (total * sconto2) / 100;
        total = total - sconto;

        if (sconto3) {
          sconto = (total * sconto3) / 100;
          total = total - sconto;
          if (quantity > 0) {
            const tot = total * quantity
            return parseFloat(tot).toFixed(2)
          }
          else {
            return total.toFixed(2)

          }
        } else {
          if (quantity > 0) {
            const tot = total * quantity
            return parseFloat(tot).toFixed(2)
          }
          else {
            return total.toFixed(2)

          }
        }
      } else {
        if (quantity > 0) {
          const tot = total * quantity
          return parseFloat(tot).toFixed(2)

        }
        else {
          return total.toFixed(2)

        }
      }
    }
    else {
      if (quantity > 0) {
        const tot = price * quantity
        return parseFloat(tot).toFixed(2)
      }
      else {
        return parseFloat(price).toFixed(2)
      }

    }
  };

  addToCart = async (name, image, price, quantity, id) => {
    const product = this.state.product[0].data;
    let actualPrice = 0;
    if (product.prezzo_promo.no_tax > 0) {
      actualPrice = product.prezzo_promo.no_tax.toString();
    } else {
      actualPrice = product.prezzo.no_tax.toString();
    }
    if (this.state.quantity <= 0) {
      Alert.alert("Inserisci una quantità");
    } else {
      // DEFINE THE PRODUCT TO STORE ON LOCAL STORAGE IF USER ADD TO CART
      let cartItem = {
        discount1: this.state.sconto1,
        discount2: this.state.sconto2,
        discount3: this.state.sconto3,
        name: name,
        image: image,
        price: price,
        quantity: quantity,
        id: id,
        actualPrice,
      };

      // CHECH IF THERE ARE OTHER PRODUCTS
      const existingProducts = await AsyncStorage.getItem("products");

      let newCart = JSON.parse(existingProducts);
      if (!newCart) {
        newCart = [];
      }
      newCart.push(cartItem);
      await AsyncStorage.setItem("products", JSON.stringify(newCart));
      Alert.alert(name + " è stato aggiunto al carrello");
      this.context?.evaluateCartItemLength();
    }
  };

  getUnit = (price) => {
    let total = this.getTotal(price)
    let unit = total / this.state.num
    if (this.state.quantity > 0) {
      unit = unit / this.state.quantity
    }
    return unit.toFixed(2)
  }

  render() {
    if (this.state.product && this.state.product.length) {
      const product = this.state.product[0].data;
      let price = 0;

      if (product.prezzo_promo.no_tax > 0) {
        price = product.prezzo_promo.no_tax.toString();
      }
      else {
        price = product.prezzo.no_tax.toString();
      }

      return (

        <KeyboardAwareScrollView
          showsVerticalScrollIndicator={false}
          style={{ width: "90%", marginLeft: "auto", marginRight: "auto" }}
          extraHeight={Dimensions.get("window").height - (Dimensions.get("window").height / 100) * 50}
          enableAutomaticScroll={true}
          enableOnAndroid={true}
        >

          <Image
            style={styles.image}
            source={{ uri: product.gallery[0].main }}
          />

          <View style={{ width: "100%" }}>
            <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{product.nome}</Text>

            <View style={{ marginTop: 20 }}>
              <HTML html={product.short_descr.substring(0, 240)} imagesMaxWidth={Dimensions.get('window').width} />
            </View>

            <View
              style={{
                marginTop: 10,
                flexDirection: "column",
                width: "100%",
                backgroundColor: "#EDEDEC",
                padding: 20,
              }}
            >

              <View style={{ flexDirection: "row" }}>
                <TextInput
                  keyboardType={"phone-pad"}
                  onChangeText={(quantity) =>
                    this.setState({ quantity: quantity })
                  }
                  placeholder={"Quantità"}
                  onEndEditing={() => this.getTotal(price)}
                  style={styles.priceInput}
                ></TextInput>
              </View>

              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  marginTop: 20,
                }}
              >
                <View style={{ flexDirection: "column", width: "30%" }}>
                  <TextInput
                    keyboardType={"phone-pad"}
                    onEndEditing={() => this.getTotal(price)}
                    onChangeText={(sconto1) =>
                      this.setState({ sconto1: sconto1 })
                    }
                    onBlur={() => this.setState({ display: "flex" })}
                    style={styles.priceInput}
                    placeholder={"Sconto 1"}
                  ></TextInput>
                </View>
                <View style={{ flexDirection: "column", width: "30%" }}>
                  <TextInput
                    keyboardType={"number-pad"}
                    onEndEditing={() => this.getTotal(price)}
                    onChangeText={(sconto2) =>
                      this.setState({ sconto2: sconto2 })
                    }
                    onBlur={() => this.setState({ display: "flex" })}
                    style={styles.priceInput}
                    placeholder={"Sconto 2"}
                  ></TextInput>
                </View>
                <View style={{ flexDirection: "column", width: "30%" }}>
                  <TextInput
                    keyboardType={"number-pad"}
                    onEndEditing={() => this.getTotal(price)}
                    onChangeText={(sconto3) =>
                      this.setState({ sconto3: sconto3 })
                    }
                    onBlur={() => this.setState({ display: "flex" })}
                    style={styles.priceInput}
                    placeholder={"Sconto 3"}
                  ></TextInput>
                </View>
              </View>

            </View>
          </View>
          <View style={{ width: "100%", alignSelf: "center" }}>
            <View
              style={{
                marginTop: 20,
                flexDirection: "column",
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <Text style={{ color: 'black', fontSize: 15,marginBottom:5 }}>
                  Per unità:{" "}
                  {this.getUnit(price)} €
                  </Text>
              </View>
              <View style={{ flexDirection: "row" }}>
                <Text style={[styles.title, { marginLeft: 0 }]}>
                  Totale:{" "}
                  {this.getTotal(price)} €
                  </Text>
              </View>
            </View>
            <TouchableOpacity
              style={styles.btn}
              onPress={() =>
                this.addToCart(
                  product.nome,
                  product.gallery[0].thumb,
                  this.getTotal(price),
                  this.state.quantity,
                  product.id
                )
              }
            >
              <Text style={{ color: "white" }}>Aggiungi all'ordine</Text>
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
      );
    } else {
      return (
        <View style={styles.container}>
          <ActivityIndicator></ActivityIndicator>
        </View>
      );
    }
  }
}

export default ProductScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "flex-start",
    justifyContent: "flex-start",
    width: "100%",
    marginLeft: "auto",
    marginRight: "auto",
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
    color: "black",
    alignSelf: "flex-start",
  },
  sub: {
    color: "black",
    fontSize: 13,
    marginBottom: "5%",
  },
  description: {
    color: "grey",
    fontWeight: "400",
    fontSize: 15,
    marginTop: "3%",
  },
  articleTitle: {
    color: "white",
    fontWeight: "bold",
    marginTop: "3%",
  },
  image: {
    width: "100%",
    height:
      Dimensions.get("window").height -
      (Dimensions.get("window").height / 100) * 65,
    resizeMode: "contain",
  },
  btn: {
    backgroundColor: "#EE0750",
    marginTop: 20,
    justifyContent: "center",
    alignItems: "center",
    padding: 15,
    borderRadius: 10,
    marginBottom: 20,
  },
  priceInput: {
    borderColor: "gray",
    borderWidth: 1,
    width: "100%",
    padding: 5,
    fontSize: 18,
    backgroundColor: "white",
  },
});
