import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, Dimensions, Image, Alert, AsyncStorage } from 'react-native';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import { cos, acc } from 'react-native-reanimated';
import { StackActions } from '@react-navigation/native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

const LoginScreen = ({ navigation }) => {
    const [email, setEmail] = useState('');
    const [password, setPsw] = useState('');

    // redirect to the home if user is already logged in
    useEffect(() => {
        checkIfLoggedIn = async () => {
            const account = await AsyncStorage.getItem('account');
            if (account !== null && account !== undefined) {
                navigation.dispatch({
                    ...StackActions.replace('Home')
                })
            }
        }
        checkIfLoggedIn();
    }, [])

    // login function
    const login = () => {
        fetch('https:/petgoo.framework360.it/m/api/users/login/?email=' + email.email + '&password=' + password.password, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'X-Fw360-Key': '9XbHPsALrgEIxhMd8N1azYtJBpSyFkv2uVQCTU5n'
            }
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status == 1) {
                    AsyncStorage.setItem('account', JSON.stringify(responseJson));
                    AsyncStorage.setItem('token', JSON.stringify(responseJson.data.access_token))
                    navigation.dispatch({
                        ...StackActions.replace('Home')
                    })
                }
                else {
                    if (responseJson.error == 'wrong_credentials') {
                        Alert.alert('Credenziali errate')
                    }
                    else if (responseJson.error = 'user_not_found') {
                        Alert.alert('Utente non trovato')
                    }
                    else {
                        Alert.alert(ùresponseJson.error)

                    }
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    return (
        <KeyboardAwareScrollView
            showsVerticalScrollIndicator={false}
            contentContainerStyle={styles.container}
            extraHeight={200}
            enableAutomaticScroll={true}
            enableOnAndroid={true}
        >
            <Image
                style={styles.image}
                source={require('../assets/pet_go.png')}
            />

            <View style={styles.titleRow}>
                <Text style={styles.title}>Benvenuto</Text>
                <Text style={styles.sub}>Effettua il login per continuare</Text>
            </View>

            <View style={styles.form}>
                <TextInput autoCapitalize={"none"} placeholder='Email' style={styles.formInput} onChangeText={(email) => setEmail({ email })} value={email}></TextInput>
                <TextInput autoCapitalize={"none"} secureTextEntry={true} placeholder='Password' style={styles.formInput} onChangeText={(password) => setPsw({ password })} value={password}></TextInput>
                <TouchableOpacity onPress={() => login()} style={styles.btn}><Text style={styles.btnText}>Accedi</Text></TouchableOpacity>
            </View>
        </KeyboardAwareScrollView>
    )
}
export default LoginScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'flex-start',
        justifyContent: 'center',
        width: '100%',
        marginLeft: 'auto'
    },
    title: {
        fontWeight: 'bold',
        fontSize: 30,
    },
    titleRow: {
        alignSelf: 'flex-start',
        width: '95%',
        marginLeft: 'auto'
    },
    sub: {
        color: 'gray',
        fontSize: 20
    },
    form: {
        marginTop: 20,
        width: '90%',
        alignSelf: 'center'
    },
    formInput: {
        backgroundColor: '#EAE9E9',
        padding: 15,
        marginTop: 20
    },
    btnText: {
        color: 'white'
    },
    btn: {
        backgroundColor: '#EE0750',
        marginTop: 40,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 15,
        borderRadius: 10
    },
    registerRow: {
        marginTop: 40,
        flexDirection: 'row',
    },
    image: {
        alignSelf: 'center',
        resizeMode: 'contain',
        width: '65%'
    }
});
