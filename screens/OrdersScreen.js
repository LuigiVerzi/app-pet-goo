import React, { Component } from "react";
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  StatusBar,
  SafeAreaView,
  Dimensions,
  FlatList,
  Image,
  ActivityIndicator,
  AsyncStorage,
} from "react-native";
import { TextInput } from "react-native-gesture-handler";
import { Ionicons } from "@expo/vector-icons";
import { FontAwesome } from "@expo/vector-icons";
import sendApiRequest from "../functions/sendApiRequest";
import { CommonActions } from '@react-navigation/native';

const featuredWidth =
  Dimensions.get("window").width - (Dimensions.get("window").width / 100) * 10;


class OrdersScreen extends Component {
  constructor() {
    super();
    this.state = {
      modalVisible: false,
      orders: [],
      ordersToShow: [],
      order: [],
      token:null
    };
  }

  componentDidMount = async () => {
    const { navigation } = this.props;
    const account = await AsyncStorage.getItem('account')
    const parsed = await JSON.parse(account)
    await this.setState({
      token:parsed.data.access_token
    });
    this.getOrdersFromApi();
  };

  // fetch all orders from api
  getOrdersFromApi = async () => {
    const that = this;
    const seller = await AsyncStorage.getItem("account");
    const accountFormatted = await JSON.parse(seller);
    const { navigation } = this.props;

    sendApiRequest(
      "/orders/list/",
      {
        seller: seller.id,
      },
      function (data) {
        if (data.error === undefined) {
          that.setState({ orders: data.data.products });
          that.setState({ ordersToShow: data.data.products });
        }
      },
      that.state.token,
      function () {
        navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [
              { name: 'Login' },
            ],
          })
        );
      }
    );
  };

  handleSearch = async (query) => {
    const formattedQuery = query;
    const data = [];
    if (formattedQuery !== '' && formattedQuery !== null && formattedQuery !== undefined)
    {
      console.log(formattedQuery)
      await this.state.orders.filter((order) => {
        if (
          order.cart.products[0].title.includes(formattedQuery) ||
          order.shipping_data.ragione_sociale.includes(formattedQuery) ||
          order.order_date.includes(formattedQuery)
        ) {
          data.push(order);
        }
      });
      this.setState({
        ordersToShow: data,
      });
    }
    else {
      this.setState({
        ordersToShow: this.state.orders,
      });
    }

  };

  render() {
    return (
      <View style={{ justifyContent: "center", alignItems: "center", flex: 1 }}>
        <StatusBar></StatusBar>
        <SafeAreaView style={{ width: "100%" }}>
          <View
            style={{
              width: "90%",
              marginLeft: "auto",
              marginRight: "auto",
              marginBottom: "5%",
            }}
          >
            <Text style={styles.title}>Ordini</Text>
          </View>
          <TextInput
            onEndEditing={() => this.handleSearch(this.state.query)}
            value={this.state.query}
            onChangeText={(query) => this.setState({ query: query })}
            style={{
              backgroundColor: "#EAE9E9",
              padding: 15,
              width: "90%",
              alignSelf: "center",
              marginBottom: "5%",
            }}
            placeholder={"Cerca..."}
          ></TextInput>
        </SafeAreaView>

        {this.state.ordersToShow.length ? (
          <FlatList
            contentContainerStyle={{
              width: featuredWidth,
              alignItems: "flex-start",
              marginTop: "2%",
            }}
            data={this.state.ordersToShow}
            showsVerticalScrollIndicator={false}
            horizontal={false}
            numColumns={1}
            keyExtractor={(x, i) => i.toString()}
            renderItem={({ item }) => (
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Order', {
                  id: item.id,
                })}
                style={{
                  borderBottomWidth: 1,
                  borderBottomColor: "#EAE9E9",
                  width: "100%",
                  padding: 10,
                  flexDirection: "row",
                }}
              >
                <View>
                  <Image
                    style={{ height: 100, width: 60, resizeMode: "contain" }}
                    source={{
                      uri: item.cart.products[0].preview,
                    }}
                  />
                </View>
                <View
                  style={{ justifyContent: "space-around", marginLeft: "2%" }}
                >
                  <Text style={{ fontWeight: "bold", fontSize: 18 }}>
                    {item.cart.products[0].title.substring(0, 25)}...
                  </Text>
                  <Text>
                    Da: {item.billing_data.ragione_sociale}
                  </Text>
                  <Text>Il: {item.order_date}</Text>
                  <Text style={{ fontWeight: "bold" }}>
                    {item.cart.total_price.no_tax.toFixed(2)} €
                  </Text>
                </View>
              </TouchableOpacity>
            )
            }
          />
        ) : (
            <View
              style={{
                alignSelf: "center",
                marginTop: "auto",
                marginBottom: "auto",
              }}
            >
              <Text>Nessun dato disponibile</Text>
            </View>
          )}

        <View
          style={{
            backgroundColor: "white",
            height: "7%",
            width: "100%",
            justifyContent: "center",
            borderTopColor: "#EAE9E9",
            borderTopWidth: 1,
          }}
        >
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-around",
              alignItems: "center",
            }}
          >
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("Home")}
              hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }}
            >
              <FontAwesome name="home" size={28} color="black" />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("Search")}
              hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }}
            >
              <FontAwesome name="group" size={28} color="black" />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("Orders")}
              hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }}
            >
              <FontAwesome name="folder" size={28} color="#EE0750" />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("Profile")}
              hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }}
            >
              <FontAwesome name="user" size={28} color="black" />
            </TouchableOpacity>
          </View>
        </View>
      </View >
    );
  }
}
export default OrdersScreen;

const styles = StyleSheet.create({
  title: {
    fontSize: 30,
    fontWeight: "bold",
    color: "black",
    alignSelf: "flex-start",
  },
});
