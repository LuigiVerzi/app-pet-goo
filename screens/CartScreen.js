import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  AsyncStorage,
  FlatList,
  Image,
  Dimensions,
  ScrollView,
  Platform,
  Modal,
  TextInput,
  TouchableOpacity,
  Alert,
  SafeAreaView,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { CartContext } from "./../Context/CartContext";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

class CartScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: null,
      total: 0,
      index: null,
      modalvisible: false,
      price: null,
      sconto1: null,
      sconto1: null,
      sconto3: null,
      quantity: null,
      actualPrice: 0,
      discountedProductPrice: 0,
      color: ''
    };
  }
  static contextType = CartContext;
  openModal = (id, index) => {
    this.setState({ modalvisible: true });
    this.setState({ index: index });

    const product = this.state.products.find((products) => products.id == id);

    this.setState({
      quantity: product.quantity,
      editedItemId: id,
      sconto1: product.discount1,
      sconto2: product.discount2,
      sconto3: product.discount3,
      actualPrice: product.actualPrice,
    });
  };

  modifyProduct = () => {
    const {
      quantity,
      sconto1,
      sconto2,
      sconto3,
      actualPrice,
      editedItemId,
      products,
    } = this.state;

    if (quantity && quantity > 0) {
      const discountedPrice = this.calculateDiscountPrice(
        actualPrice,
        sconto1,
        sconto2,
        sconto3,
        quantity
      );

      const updatedProduct = {
        discount1: sconto1,
        discount2: sconto2,
        discount3: sconto3,
        quantity,
        price: discountedPrice * quantity,
      };

      const updatedProductArray = products.map((_o) => {
        if (_o.id === editedItemId) {
          return {
            ..._o,
            ...updatedProduct,
          };
        } else {
          return _o;
        }
      });
      this.setState(
        {
          products: updatedProductArray,
          modalvisible: false,
          editedItemId: null,
          price: null,
          sconto1: null,
          sconto1: null,
          sconto3: null,
          quantity: null,
          actualPrice: 0,
        },
        () => {
          (async () => {
            await AsyncStorage.setItem(
              "products",
              JSON.stringify(this.state.products)
            );
            this.calculateTotal();
          })();
        }
      );
    } else {
      Alert.alert("Quantity should be at least 1.");
    }
  };
  // CALUCLATE THE TOTAL PRICE
  calculateTotal = () => {
    let i = 0;
    let total = 0;

    for (i = 0; i < this.state.products.length; i++) {
      total = this.state.products[i].price + total;
    }

    this.setState({ total: total });
  };

  // REMOVE ITEM FROM THE CART
  async removeItemValue(key) {
    try {
      // UPDATE THE CART
      const items = this.state.products.filter(
        (products) => products.id !== key.id
      );
      await this.setState({ products: items });

      // UPDATE THE TOTAL PRICE
      this.calculateTotal();

      // REMOVE ITEM FROM THE STORAGE
      await AsyncStorage.setItem(
        "products",
        JSON.stringify(this.state.products)
      );
      this.context?.evaluateCartItemLength();
      if (this.state.products.length == 0) {
        this.setState({
          color: 'grey'
        });
      }
    } catch (exception) {
      return false;
    }
  }

  closeModal = () => {
    this.setState({ modalvisible: false });
  };

  checkout = () => {
    if (this.state.products.length > 0) {
      this.props.navigation.navigate("Checkout")
    }
    else {
      Alert.alert('Aggiungi almeno un prodotto al carrello')
    }
  }
  // ON CART SCREEN
  componentDidMount = async () => {
    // GET ITEM FROM ASYNCSTORAGE
    try {
      let products = await AsyncStorage.getItem("products");
      let parsed = JSON.parse([products]);
      if (parsed.length > 0) {
        this.setState({
          products: parsed,
          color: '#EE0750'
        });
      }
      else {
        this.setState({
          products: parsed,
          color: 'grey'
        });
      }
      this.setState({ products: parsed ? parsed : [] });
    } catch (error) {
      console.log(error);
    }

    // CALCULATE THE TOTAL
    this.calculateTotal();
  };

  calculateDiscountPrice = (actualPrice, discount1, discount2, discount3) => {
    let price = actualPrice;
    let sconto1 = discount1;
    let sconto2 = discount2;
    let sconto3 = discount3;

    let sconto;
    let total;

    if (sconto1) {
      sconto = (price * sconto1) / 100;
      total = price - sconto;

      if (sconto2) {
        sconto = (total * sconto2) / 100;
        total = total - sconto;

        if (sconto3) {
          sconto = (total * sconto3) / 100;
          total = total - sconto;
          return total.toFixed(2);
        } else {
          return total.toFixed(2);
        }
      } else {
        return total.toFixed(2);
      }
    } else {
      return (total = price);
    }
  };
  onCartItemEditChangeDiscountPrice = () => {
    const { quantity, sconto1, sconto2, sconto3, actualPrice } = this.state;
    return this.calculateDiscountPrice(actualPrice, sconto1, sconto2, sconto3)
  };

  formattedTotal = () => {
    let total = this.onCartItemEditChangeDiscountPrice() * this.state.quantity;
    let formatted = parseFloat(total).toFixed(2)
    return formatted
  }

  render() {
    return (
      <View style={styles.container}>
        <Text
          style={{
            color: "black",
            fontSize: 18,
            fontWeight: "bold",
            marginLeft: "5%",
            marginTop: "5%",
          }}
        >
          {" "}
          Totale: {parseFloat(this.state.total).toFixed(2)} €
        </Text>
        <FlatList
          contentContainerStyle={{
            width: "100%",
            borderColor: "white",
            borderWidth: 1,
            marginTop: "10%",
          }}
          numColumns={1}
          horizontal={false}
          data={this.state.products}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({ item, index }) => (
            <TouchableOpacity
              style={styles.productBox}
              onLongPress={() => this.openModal(item.id, index)}
            >
              <Image style={styles.productImage} source={{ uri: item.image }} />
              <View
                style={{
                  flexDirection: "column",
                  marginLeft: 10,
                  width: "70%",
                }}
              >
                <Text style={styles.productTitle}>{item.name}</Text>

                <Text style={styles.productPrice}>
                  {" "}
                  {parseFloat(item.price).toFixed(2)} €
                </Text>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    marginTop: 10,
                  }}
                >
                  <Text style={styles.productQuantity}>
                    {" "}
                    {item.quantity} stock selezionati
                  </Text>
                  <TouchableOpacity
                    onPress={() => this.openModal(item.id, index)}
                    style={{
                      backgroundColor: "#EE0750",
                      paddingTop: 5,
                      paddingBottom: 5,
                      paddingLeft: 12,
                      paddingRight: 12,
                    }}
                  >
                    <Ionicons name="md-create" size={24} color="white" />
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => this.removeItemValue(item)}
                    style={{
                      backgroundColor: "#EE0750",
                      paddingTop: 5,
                      paddingBottom: 5,
                      paddingLeft: 15,
                      paddingRight: 15,
                    }}
                  >
                    <Ionicons name="ios-trash" size={24} color="#ffff" />
                  </TouchableOpacity>
                </View>
              </View>
            </TouchableOpacity>
          )}
        />
        <TouchableOpacity
          onPress={() => this.checkout()}
          style={[styles.btn, { backgroundColor: this.state.color }]}
        >
          <Text style={styles.btnText}>Completa L'ordine</Text>
        </TouchableOpacity>

        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalvisible}
        >
          <KeyboardAwareScrollView
            showsVerticalScrollIndicator={false}
            style={{
              flex: 1,
              backgroundColor: "white",
              opacity: 1,
              flexDirection: "column",
            }}
            enableOnAndroid={false}
            enableAutomaticScroll={(Platform.OS === 'ios')}

            contentContainerStyle={{ flexGrow: 1, justifyContent: 'space-evenly', alignItems: 'center' }}
          >


              <SafeAreaView></SafeAreaView>


              <Text style={{ color: "black", fontWeight: "bold", fontSize: 20 }}>
                Modifica il prodotto
              </Text>

              <View style={{ width: "100%", alignItems: "center" }}>
                <Text style={{ color: 'black', fontSize: 18, alignSelf: 'flex-start', marginLeft: '10%', marginBottom: 10 }}>Quantità</Text>

                <TextInput
                  value={this.state.quantity}
                  onChangeText={(quantity) =>
                    this.setState({ quantity: quantity })
                  }
                  placeholderTextColor={"black"}
                  style={{
                    borderColor: "black",
                    borderBottomWidth: 1,
                    padding: 10,
                    width: "80%",
                    marginBottom: 20,
                    color: "black",
                  }}
                ></TextInput>
                <Text style={{ color: 'black', fontSize: 18, alignSelf: 'flex-start', marginLeft: '10%', marginBottom: 10 }}>Sconto 1</Text>

                <TextInput
                  value={this.state.sconto1}
                  onChangeText={(sconto1) => this.setState({ sconto1: sconto1 })}
                  placeholderTextColor={"black"}
                  style={{
                    borderColor: "black",
                    borderBottomWidth: 1,
                    padding: 10,
                    width: "80%",
                    marginBottom: 20,
                    color: "black",
                  }}
                ></TextInput>
                <Text style={{ color: 'black', fontSize: 18, alignSelf: 'flex-start', marginLeft: '10%', marginBottom: 10 }}>Sconto 2</Text>

                <TextInput
                  value={this.state.sconto2}
                  onChangeText={(sconto2) => this.setState({ sconto2: sconto2 })}
                  placeholderTextColor={"black"}
                  style={{
                    borderColor: "black",
                    borderBottomWidth: 1,
                    padding: 10,
                    width: "80%",
                    marginBottom: 20,
                    color: "black",
                  }}
                ></TextInput>
                <Text style={{ color: 'black', fontSize: 18, alignSelf: 'flex-start', marginLeft: '10%', marginBottom: 10 }}>Sconto 3</Text>

                <TextInput
                  value={this.state.sconto3}
                  onChangeText={(sconto3) => this.setState({ sconto3: sconto3 })}
                  placeholderTextColor={"black"}
                  style={{
                    borderColor: "black",
                    borderBottomWidth: 1,
                    padding: 10,
                    width: "80%",
                    marginBottom: 20,
                    color: "black",
                  }}
                ></TextInput>
              </View>

              <Text
                style={{ color: "black", fontWeight: "bold", fontSize: 18, }}
              >
                Totale:{" "}
                {this.formattedTotal()}
              </Text>
              <View>
                <TouchableOpacity
                  onPress={() => this.modifyProduct()}
                  style={{
                    backgroundColor: "#EE0750",
                    padding: 10,
                    justifyContent: "center",
                    alignItems: "center",
                    borderRadius: 10,
                    width: 200,
                  }}
                >
                  <Text style={{ fontWeight: "700", fontSize: 18, color: 'white' }}>Salva</Text>
                </TouchableOpacity>


                <TouchableOpacity
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                  onPress={() => this.closeModal()}
                >
                  <Text
                    style={{ fontWeight: "500", fontSize: 20, color: "black", marginTop: 10 }}
                  >
                    indietro
                </Text>
                </TouchableOpacity>
              </View>
          </KeyboardAwareScrollView>

        </Modal >
      </View >
    );
  }
}
export default CartScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  productBox: {
    flexDirection: "row",
    borderBottomWidth: 1,
    borderColor: "grey",
    width: "90%",
    marginLeft: "auto",
    marginRight: "auto",
    paddingBottom: 10,
    marginBottom: "10%",
  },
  productImage: {
    height: 'auto',
    width: "25%",
  },
  productTitle: {
    color: "black",
    fontSize: 18,
    fontWeight: "400",
    flexShrink: 1,
  },
  productPrice: {
    fontWeight: "600",
    fontSize: 18,
  },
  productQuantity: {
    color: "#EE0750",
    marginTop: 7
  },
  btnText: {
    color: "white",
  },
  btn: {
    justifyContent: "center",
    alignItems: "center",
    padding: 15,
    borderRadius: 10,
    marginBottom: 20,
    width: "90%",
    alignSelf: "center",
  },
});