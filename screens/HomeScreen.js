import React from "react";
import { StyleSheet, Text, View, FlatList, Image, Dimensions, TouchableOpacity, ActivityIndicator, TextInput, AsyncStorage, } from "react-native";
import { FontAwesome } from "@expo/vector-icons";
import sendApiRequest from "../functions/sendApiRequest";
import { CommonActions } from '@react-navigation/native';

const featuredWidth = Dimensions.get("window").width - (Dimensions.get("window").width / 100) * 10;


export default class HomeScreen extends React.Component {
  state = {
    query: '',
    products: null,
    refreshing: false,
    page: 1,
    token: null,
    refreshing: false
  };


  nextPage = async () => {
    console.log('trigger')
    if (this.state.query !== '') {
    }
    else {
      await this.setState({ page: this.state.page + 1 });
      const that = this;
      sendApiRequest(
        "/ecommerce/products/list/",
        {
          page: this.state.page,
          order: ['manual', 'ASC'],
        },
        async (data) => {
          if (typeof data.error == "undefined") {
            that.setState({
              products: [...this.state.products, ...data.data.products]
            })
          }
        },
        that.state.token,
        function () {
          this.props.navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [
                { name: 'Login' },
              ],
            })
          );
        }
      )
    }
  }

  componentDidMount = async () => {
    const account = await AsyncStorage.getItem('account')
    const parsed = await JSON.parse(account)
    await this.setState({
      token: parsed.data.access_token
    });
    this.searchProductsFromApi();
  }

  // query a product from api
  searchProductsFromApi = async (page) => {
    const that = this;
    let pagina;

    if (page) {
      pagina = page
      this.setState({ page: page })
    }
    else {
      pagina = this.state.page
    }

    sendApiRequest(
      "/ecommerce/products/list/",
      {
        query: this.state.query,
        order: ['manual', 'ASC'],
        page: pagina
      },
      function (data) {
        console.log(data)
        if (typeof data.error == "undefined") {
          that.setState({ products: data.data.products });
        }
        else {
          that.setState({
            products: [],
          });
        }
      },
      that.state.token,
      function () {
        this.props.navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [
              { name: 'Login' },
            ],
          })
        );
      }
    );

  };

  render() {
    if (this.state.products === null) {
      return (
        <View style={styles.container}>
          <ActivityIndicator></ActivityIndicator>

        </View>
      );
    }
    else {
      return (
        <View style={styles.body}>
          <View style={styles.container}>
            <TextInput
              onEndEditing={() => this.searchProductsFromApi(1)}
              value={this.state.query}
              onChangeText={(query) => this.setState({ query: query })}
              style={{
                backgroundColor: "#EAE9E9",
                padding: 10,
                width: "100%",
                alignSelf: "center",
                marginTop: 20,
                marginBottom: 20,
              }}
              placeholder={"Cerca..."}
            ></TextInput>

            <FlatList
              contentContainerStyle={{ width: featuredWidth }}
              columnWrapperStyle={{ justifyContent: "space-between" }}
              data={this.state.products}
              showsVerticalScrollIndicator={false}
              horizontal={false}
              refreshing={this.state.refreshing}
              numColumns={2}
              onEndReachedThreshold={1}
              onEndReached={this.nextPage}
              keyExtractor={(x, i) => i}
              renderItem={({ item }) => (
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate("Product", {
                      id: item.id,
                    })
                  }
                  style={{
                    width: featuredWidth - (featuredWidth / 100) * 54,
                    marginTop: "8%",
                  }}
                >
                  <Image
                    style={styles.image}
                    source={{ uri: item.gallery[0].thumb }}
                  />
                  <Text style={[styles.author]}>{item.nome}</Text>
                  <Text
                    style={{ fontWeight: "bold", marginTop: 5, fontSize: 15 }}
                  >
                    {item.prezzo_promo.no_tax > 0
                      ? item.prezzo_promo.no_tax.toFixed(2)
                      : item.prezzo.no_tax.toFixed(2)}{" "}
                    €
                  </Text>
                </TouchableOpacity>
              )}
            />
          </View>
          <View
            style={{
              backgroundColor: "white",
              height: "7%",
              width: "100%",
              justifyContent: "center",
              borderTopColor: "#EAE9E9",
              borderTopWidth: 1,
            }}
          >
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-around",
                alignItems: "center",
              }}
            >
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("Home")}
                hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }}
              >
                <FontAwesome name="home" size={28} color="#EE0750" />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("Search")}
                hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }}
              >
                <FontAwesome name="group" size={28} color="black" />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("Orders")}
                hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }}
              >
                <FontAwesome name="folder" size={28} color="black" />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("Profile")}
                hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }}
              >
                <FontAwesome name="user" size={28} color="black" />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  body: {
    flex: 1,
    width: "100%",
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    marginLeft: "auto",
    marginRight: "auto",
  },
  productBox: {
    borderWidth: 1,
    borderColor: "red",
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    width: "90%",
    marginLeft: "auto",
    marginRight: "auto",
  },
  title: {
    fontSize: 30,
    fontWeight: "bold",
    color: "black",
    alignSelf: "flex-start",
  },
  sub: {
    color: "black",
    fontSize: 13,
    marginBottom: "5%",
  },
  author: {
    color: "black",
    fontWeight: "400",
    fontSize: 15,
    marginTop: "3%",
  },
  articleTitle: {
    color: "white",
    fontWeight: "bold",
    marginTop: "3%",
  },
  image: {
    width: "100%",
    height: 250,
    resizeMode: "contain",
  },
});
