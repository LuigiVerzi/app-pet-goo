import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Text, Modal, SafeAreaView, Dimensions, FlatList, Image, ActivityIndicator } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

class ThankYouScreen extends Component {

    componentDidMount = () => {
        const { navigation } = this.props;
    }

    render() {
        return (
            <View style={{ justifyContent: 'space-evenly', alignItems: 'center', flex: 1 }}>
                <Text style={{ color: 'black', fontSize: 25, fontWeight: '600', width: '80%', textAlign: 'center' }}>Grazie, ordine effettuato con successo!</Text>
                <FontAwesome name="check-circle" size={200} color="#1FC418" />
                <TouchableOpacity style={styles.btn} onPress={() => this.props.navigation.navigate('Orders')}>
                    <Text style={{ color: 'white', fontSize: 18 }}>I miei ordini</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
export default ThankYouScreen;

const styles = StyleSheet.create({
    btn: {
        backgroundColor: '#EE0750',
        marginTop: 40,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 15,
        borderRadius: 10,
        width: '90%'
    },

});