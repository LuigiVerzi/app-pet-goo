import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Text, Modal, SafeAreaView, Dimensions, FlatList, StatusBar, ActivityIndicator, AsyncStorage } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { FontAwesome } from '@expo/vector-icons';

import sendApiRequest from '../functions/sendApiRequest';
import { cos } from 'react-native-reanimated';
import { CommonActions } from '@react-navigation/native';

const featuredWidth = Dimensions.get('window').width - Dimensions.get('window').width / 100 * 10;

/* function sendApiRequest(endpoint, data, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open("POST", 'https://petgoo.framework360.it/m/api' + endpoint, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.setRequestHeader('X-Fw360-Key', '9XbHPsALrgEIxhMd8N1azYtJBpSyFkv2uVQCTU5n');
    xhr.send(JSON.stringify(data));
    xhr.onload = function () {
        callback(JSON.parse(this.responseText));
    }
}
*/

class ClientsScreen extends Component {
    constructor() {
        super();
        this.state = {
            query: null,
            modalVisible: false,
            nome: '',
            cognome: '',
            città: '',
            data_di_nascita: '',
            comune: '',
            cellulare: '',
            indirizzo: '',
            ragione_sociale: '',
            email: '',
            telefono: '',
            piva: '',
            token: null,
            clients: [

            ]
        };
    }

    componentDidMount = async () => {
        const { navigation } = this.props;
        const account = await AsyncStorage.getItem('account')
        const parsed = await JSON.parse(account)
        await this.setState({
            token: parsed.data.access_token
        });
        this.searchClients();
    }

    openModal = (nome, cognome, email, telefono_cellulare, telefono_casa, cap, città, comune, data_nascita, indirizzo, ragione_sociale, piva) => {
        this.setState({
            nome: nome,
            cognome: cognome,
            email: email,
            telefono: telefono_casa,
            piva: piva,
            città: città,
            data_di_nascita: data_nascita,
            comune: comune,
            cap: cap,
            data_di_nascita: data_nascita,
            cellulare: telefono_cellulare,
            indirizzo: indirizzo,
            ragione_sociale: ragione_sociale,
            modalVisible: true
        });
    }


    searchClients = () => {
        const that = this;
        const { navigation } = this.props;

        sendApiRequest('/customers/search/', {
            query: this.state.query
        }, function (data) {
            if (typeof (data.error) == 'undefined') {
                that.setState({ clients: data.data });
            }
            else {
                console.log(data)
                that.setState({
                    clients: [

                    ]
                })
            }
        },
            that.state.token,
            function () {
                navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [
                            { name: 'Login' },
                        ],
                    })
                );
            }
        );

    }



    render() {
        return (
            <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>

                <SafeAreaView style={{ width: '100%' }}>
                    <StatusBar></StatusBar>
                    <View style={{ width: '90%', marginLeft: 'auto', marginRight: 'auto', marginBottom: '5%' }}>
                        <Text style={styles.title}>Clienti</Text>
                    </View>
                    <TextInput onEndEditing={() => this.searchClients()} value={this.state.query} onChangeText={(query) => this.setState({ query: query })} style={{ backgroundColor: '#EAE9E9', padding: 15, width: '90%', alignSelf: 'center' }} placeholder={'Cerca...'}></TextInput>
                </SafeAreaView>

                {this.state.clients.length ?
                    <FlatList
                        contentContainerStyle={{ width: Dimensions.get('window').width - Dimensions.get('window').width / 100 * 10, marginTop: '5%' }}
                        data={this.state.clients}
                        showsVerticalScrollIndicator={false}
                        horizontal={false}
                        keyExtractor={(x, i) => i.toString()}
                        renderItem={({ item }) =>
                            (
                                <TouchableOpacity onPress={() => {
                                    /* 1. Navigate to the Details route with params */
                                    console.log(item),
                                        this.props.navigation.navigate('Client', {
                                            email: item.email,
                                            cellulare: item.telefono_cellulare,
                                            telefono: item.telefono_casa,
                                            cap: item.cap,
                                            comune: item.comune,
                                            città: item.citta,
                                            indirizzo: item.dati_fatturazione.indirizzo,
                                            piva: item.dati_fatturazione.piva,
                                            ragione_sociale: item.dati_fatturazione.ragione_sociale,
                                            cap_fatturazione: item.dati_fatturazione.cap,
                                            comune_fatturazione: item.dati_fatturazione.comune,
                                            provincia_fatturazione: item.dati_fatturazione.citta,
                                            indirizzo_fatturazione: item.dati_fatturazione.indirizzo
                                        });
                                }}
                                    style={{ borderBottomWidth: 1, borderBottomColor: '#EAE9E9', width: '100%', padding: 10, marginTop: '5%' }}>
                                    <Text style={{ fontWeight: 'bold', fontSize: 20, }}>{item.dati_fatturazione.ragione_sociale} </Text>
                                </TouchableOpacity>
                            )}
                    />
                    :
                    <View
                        style={{
                            alignSelf: "center",
                            marginTop: "auto",
                            marginBottom: "auto",
                        }}
                    >
                        <Text>Nessun dato disponibile</Text>
                    </View>}

                <View style={{ backgroundColor: 'white', height: '7%', width: '100%', justifyContent: 'center', borderTopColor: '#EAE9E9', borderTopWidth: 1 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                        <TouchableOpacity hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }} onPress={() => this.props.navigation.navigate('Home')}>
                            <FontAwesome name="home" size={28} color="black" />
                        </TouchableOpacity>

                        <TouchableOpacity hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }} onPress={() => this.props.navigation.navigate('Search')}>
                            <FontAwesome name="group" size={28} color="#EE0750" />
                        </TouchableOpacity>
                        <TouchableOpacity hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }} onPress={() => this.props.navigation.navigate('Orders')}>
                            <FontAwesome name="folder" size={28} color="black" />
                        </TouchableOpacity>
                        <TouchableOpacity hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }} onPress={() => this.props.navigation.navigate('Profile')}>
                            <FontAwesome name="user" size={28} color="black" />
                        </TouchableOpacity>

                    </View>
                </View>


            </View>
        );
    }
}
export default ClientsScreen;

const styles = StyleSheet.create({

    title: {
        fontSize: 30,
        fontWeight: 'bold',
        color: 'black',
        alignSelf: 'flex-start'

    },
    field: {
        borderBottomWidth: 1,
        borderBottomColor: '#EAE9E9',
        width: '100%',
        padding: 10,
        marginBottom: 10,
        marginTop: 10
    }

});