import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity, } from "react-native";
import { ScrollView } from "react-native-gesture-handler";


class ClientsScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            cellulare: '',
            telefono: '',
            cap: '',
            comune: '',
            indirizzo: '',
            piva: '',
            ragione_sociale: '',
            comune_fatturazione: '',
            provincia_fatturazione: '',
            cap_fatturazione: '',
            città: '',
            indirizzo_fatturazione: ''
        };
    }

    componentDidMount = async (navigation) => {
        const { email, cellulare, telefono, cap, comune, indirizzo, piva, ragione_sociale, cap_fatturazione, provincia_fatturazione, comune_fatturazione, indirizzo_fatturazione, città } = await this.props.route.params
        this.setState({
            email: email,
            cellulare: cellulare,
            telefono: telefono,
            cap: cap,
            comune: comune,
            città: città,
            indirizzo: indirizzo,
            piva: piva,
            ragione_sociale: ragione_sociale,
            comune_fatturazione: comune_fatturazione,
            indirizzo_fatturazione: indirizzo_fatturazione,
            provincia_fatturazione: provincia_fatturazione,
            cap_fatturazione: cap_fatturazione
        })
    }

    render() {
        return (
            <ScrollView horizontal={false} style={{ flex: 1 }}>
                <View style={{ width: '95%', marginLeft: 'auto', marginTop: '10%' }}>
                    <Text style={{ color: 'black', fontWeight: 'bold', fontSize: 25 }}>Dati di fatturazione:</Text>
                </View>
                <View style={[styles.field]}>
                    <Text style={styles.fieldLabel}> Ragione sociale: {this.state.ragione_sociale ? this.state.ragione_sociale : 'nessun dato trovato'}</Text>
                </View>
                <View style={styles.field}>
                    <Text style={styles.fieldLabe}> Piva: {this.state.piva ? this.state.piva : 'nessun dato trovato'}</Text>
                </View>
                <View style={[styles.field]}>
                    <Text style={styles.fieldLabe}> Provincia: {this.state.provincia_fatturazione ? this.state.provincia_fatturazione : 'nessun dato trovato'}</Text>
                </View>

                <View style={[styles.field]}>
                    <Text style={styles.fieldLabe}> Comune: {this.state.comune_fatturazione ? this.state.comune_fatturazione : 'nessun dato trovato'}</Text>
                </View>

                <View style={[styles.field]}>
                    <Text style={styles.fieldLabe}> Cap: {this.state.cap_fatturazione ? this.state.cap_fatturazione : 'nessun dato trovato'}</Text>
                </View>
                <View style={[styles.field]}>
                    <Text style={styles.fieldLabe}> Indirizzo: {this.state.indirizzo_fatturazione ? this.state.indirizzo_fatturazione : 'nessun dato trovato'}</Text>
                </View>
                <View style={{ width: '95%', marginLeft: 'auto', marginTop: '5%' }}>
                    <Text style={{ color: 'black', fontWeight: 'bold', fontSize: 25 }}>Dati Cliente:</Text>
                </View>
                <View style={styles.field}>
                    <Text style={styles.fieldLabe}> Email: {this.state.email ? this.state.email : 'nessun dato trovato'}</Text>
                </View>
                <View style={styles.field}>
                    <Text style={styles.fieldLabe}> Cellulare: {this.state.cellulare ? this.state.cellulare : 'nessun dato trovato'}</Text>
                </View>
                <View style={styles.field}>
                    <Text style={styles.fieldLabe}> Telefono: {this.state.telefono ? this.state.telefono : 'nessun dato trovato'}</Text>
                </View>
                <View style={styles.field}>
                    <Text style={styles.fieldLabe}> Provincia: {this.state.città ? this.state.città : 'nessun dato trovato'}</Text>
                </View>
                <View style={styles.field}>
                    <Text style={styles.fieldLabe}> Comune: {this.state.comune ? this.state.comune : 'nessun dato trovato'}</Text>
                </View>
                <View style={styles.field}>
                    <Text style={styles.fieldLabe}> Cap: {this.state.cap ? this.state.cap : 'nessun dato trovato'}</Text>
                </View>
                <View style={styles.field}>
                    <Text style={styles.fieldLabe }> Indirizzo: {this.state.indirizzo ? this.state.indirizzo : 'nessun dato trovato'}</Text>
                </View>

                <View>
                    <TouchableOpacity onPress={({ navigation }) => this.props.navigation.goBack()} style={{ alignSelf: 'center', justifyContent: 'center', alignItems: 'center', marginTop: '5%', marginBottom: '10%' }}>
                        <Text style={{ color: '#EE0750', fontSize: 20, fontWeight: '500' }}>Torna indietro</Text>
                    </TouchableOpacity>
                </View>


            </ScrollView>
        );
    }
}
export default ClientsScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    fieldLabel: {
        fontWeight: '500', 
        fontSize: 18
    },
    field: {
        borderBottomWidth: 1,
        borderBottomColor: '#EAE9E9',
        width: '95%',
        padding: 10,
        marginBottom: 10,
        marginTop: 10,
        alignSelf: 'center'
    }

});