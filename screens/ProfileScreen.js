import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Image, SafeAreaView, AsyncStorage, StatusBar, Button } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

import { StackActions } from '@react-navigation/native';
import { CommonActions } from '@react-navigation/native';

import sendApiRequest from "../functions/sendApiRequest";



class ProfileScreen extends React.Component {
    state = {
        account: [

        ],
        capoarea: null,
        token: null
    };
    logout = () => {
        AsyncStorage.removeItem('account');        

        this.props.navigation.dispatch(
            CommonActions.reset({
                index: 0,
                routes: [
                    { name: 'Login' },
                ],
            })
        );
    }
    componentDidMount = async () => {
        const profile = await AsyncStorage.getItem('account');
        const parsed = await JSON.parse(profile)
        await this.setState({
            token: parsed.data.access_token
        });
        const that = this;
        await this.setState({
            account: JSON.parse(profile)
        });
        sendApiRequest('/users/userReferer/get/', {
            id: that.state.account.data.id
        }, function (data) {
            if (data.error) {
                that.setState({
                    capoarea: false
                })
            }
            else {
                that.setState({
                    capoarea: true
                })
            }
        },
            that.state.token,
            function () {
                that.logout()
            }

        );


    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar></StatusBar>
                <SafeAreaView style={{ width: '100%' }}>
                    <View style={{ width: '90%', marginLeft: 'auto', marginRight: 'auto', marginBottom: '5%' }}>
                        <Text style={styles.headertitle}>Profilo</Text>
                    </View>
                </SafeAreaView>
                {this.state.account.data ?
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <View style={[styles.row, { justifyContent: 'flex-start', marginLeft: 0 }]}>
                            <Image
                                style={{ height: 100, width: 100, resizeMode: 'contain', borderRadius: 100 }}
                                source={{
                                    uri: this.state.account.data.avatar
                                }}
                            />
                        </View>

                        <View style={styles.row}>
                            <Text style={styles.title}>{this.state.account.data.nome} {this.state.account.data.cognome}</Text>
                        </View>
                        {this.state.capoarea == true ?
                            <View style={styles.row}>
                                <Text style={[styles.title, { color: '#EE0750' }]}>capo area</Text>
                            </View>
                            :
                            <Text></Text>
                        }
                        <View style={styles.row}>
                            <Text style={styles.sub}>{this.state.account.data.email}</Text>
                        </View>

                        <View style={styles.row}>
                            <Text style={styles.sub}>{this.state.account.data.cellulare}</Text>
                        </View>
                        <View style={{ marginTop: 10 }} >
                            <Button onPress={() => this.logout()} title={'logout'} color={'#EE0750'} />
                        </View>
                    </View>

                    :
                    <Text></Text>
                }
                <View style={{ backgroundColor: 'white', height: '7%', width: '100%', justifyContent: 'center', borderTopColor: '#EAE9E9', borderTopWidth: 1, marginTop: 'auto' }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                        <TouchableOpacity hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }} onPress={() => this.props.navigation.navigate('Home')}>
                            <FontAwesome name="home" size={28} color="black" />
                        </TouchableOpacity>
                        <TouchableOpacity hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }} onPress={() => this.props.navigation.navigate('Search')}>
                            <FontAwesome name="group" size={28} color="black" />
                        </TouchableOpacity>
                        <TouchableOpacity hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }} onPress={() => this.props.navigation.navigate('Orders')}>
                            <FontAwesome name="folder" size={28} color="black" />
                        </TouchableOpacity>
                        <TouchableOpacity hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }} onPress={() => this.props.navigation.navigate('Profile')}>
                            <FontAwesome name="user" size={28} color="#EE0750" />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )

    }
}



export default ProfileScreen;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    row: {
        flexDirection: 'row',
        marginTop: 15,
        marginLeft: '2%',


    },
    title: {
        fontWeight: 'bold',
        fontSize: 22
    },
    sub: {
        fontWeight: '700',
        color: 'grey',
        fontSize: 16
    },
    headertitle: {
        fontSize: 30,
        fontWeight: 'bold',
        color: 'black',
        alignSelf: 'flex-start'

    },
});
