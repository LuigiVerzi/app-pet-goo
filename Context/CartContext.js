import React, { createContext, useContext, useState } from "react";
import { AsyncStorage } from "react-native";
export const CartContext = createContext();

export const CartConsumer = CartContext.Consumer;

export const useCartContext = () => {
  return useContext(CartContext);
};

const getProviderValue = () => {
  const [cartLength, setCartLength] = useState(0);

  const evaluateCartItemLength = async () => {
    // GET THE PRODUCTS STRING FROM ASYNCSTORAGE
    let products = await AsyncStorage.getItem("products");

    // CONVERT IT INTO AN OBJECT
    let parsed = JSON.parse([products]);
    setCartLength(parsed?.length);
  };
  const clearCartProduct = async () => {
    await AsyncStorage.removeItem("products");
    setCartLength(0);
  };
  return { cartLength, evaluateCartItemLength, clearCartProduct };
};
export function CartContextProvider({ children }) {
  const data = getProviderValue();
  return <CartContext.Provider value={data}>{children}</CartContext.Provider>;
}
export default CartContext;
