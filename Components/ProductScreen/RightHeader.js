import React, { useEffect } from "react";
import { Text, View, TouchableOpacity } from "react-native";
import { useCartContext } from "../../Context/CartContext";

import { Ionicons } from "@expo/vector-icons";

const RightHeader = (props) => {
  const cartContextData = useCartContext();

  useEffect(() => {
    cartContextData?.evaluateCartItemLength();
  }, []);
  return (
    <TouchableOpacity
      hitSlop={{ top: 20, bottom: 20, left: 70, right: 50 }}
      style={{ marginRight: 20, marginTop: -5 }}
    >
      <View
        style={{ backgroundColor: "#EE0750", borderRadius: 10, marginLeft: 10 }}
      >
        <Text style={{ color: "white", textAlign: "center", fontSize: 10 }}>
          {cartContextData?.cartLength || 0}
        </Text>
      </View>
      <Ionicons
        name="ios-cart"
        size={30}
        color="#EE0750"
        onPress={() => props.navigation.navigate("Cart")}
      />
    </TouchableOpacity>
  );
};

export default RightHeader;
