import { StatusBar } from "expo-status-bar";
import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  AsyncStorage,
} from "react-native";
//SCREENS
import LoginScreen from "./screens/LoginScreen";
import HomeScreen from "./screens/HomeScreen";
import ProductScreen from "./screens/ProductScreen";
import CartScreen from "./screens/CartScreen";
import CheckoutScreen from "./screens/CheckoutScreen";
import SearchScreen from "./screens/SearchScreen";
import ThankYouScreen from "./screens/ThankYouScreen";
import OrdersScreen from "./screens/OrdersScreen";
import ProfileScreen from "./screens/ProfileScreen";
import ClientScreen from "./screens/ClientScreen";
import SingleOrder from './screens/SingleOrder';

// FOR THE NAVIGATION
import { createAppContainer } from "react-navigation";
import createAnimatedSwitchNavigator from "react-navigation-animated-switch";
import { Transition } from "react-native-reanimated";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer, DefaultTheme } from "@react-navigation/native";
import { RightHeaderProductScreen } from "./Components/ProductScreen";

import { CartContextProvider } from "./Context/CartContext";
const MyTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: "#EE0750",
    background: "white",
  },
};

const Stack = createStackNavigator();

class App extends React.Component {
  render() {
    return (
      <NavigationContainer theme={MyTheme}>
        <CartContextProvider>
          <Stack.Navigator>
            <Stack.Screen
              name="Login"
              component={LoginScreen}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="Home"
              component={HomeScreen}
              options={({ navigation, route }) => ({
                headerLeft: (props) => (
                  <Text style={{ fontSize: 30, fontWeight: 'bold', marginLeft: 20 }}>Prodotti</Text>
                ),
                headerRight: (props) => (
                  <RightHeaderProductScreen
                    {...props}
                    navigation={navigation}
                    route={route}
                  />
                ),
                title: ''
              })}
            />
            <Stack.Screen
              name="Product"
              component={ProductScreen}
              options={({ navigation, route }) => ({
                headerRight: (props) => (
                  <RightHeaderProductScreen
                    {...props}
                    navigation={navigation}
                    route={route}
                  />
                ),
                headerTitle: (props) => (
                  <Text style={{ fontSize: 20, fontWeight: "700" }}>
                    PET GOO
                  </Text>
                ),
                headerBackTitle: 'Indietro'
              })}
            />
            <Stack.Screen
              name="Cart"
              component={CartScreen}
              options={{ headerTitle: "Carrello", headerBackTitle: "Indietro" }}
            />
            <Stack.Screen
              name="Client"
              component={ClientScreen}
              options={{ headerBackTitle: "Indietro", headerTitle: 'DATI' }}
            />
            <Stack.Screen name="Checkout" component={CheckoutScreen} />
            <Stack.Screen
              name="Search"
              component={SearchScreen}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="ThankYou"
              component={ThankYouScreen}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="Orders"
              component={OrdersScreen}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="Order"
              component={SingleOrder}
              options={{ title: 'Ordine', headerBackTitle: 'Indietro' }}

            />
            <Stack.Screen
              name="Profile"
              component={ProfileScreen}
              options={{ headerShown: false }}
            />
          </Stack.Navigator>
        </CartContextProvider>
      </NavigationContainer>
    );
  }
}

export default App;

/*
const AppContainer = createAnimcreateStackNavigatoratedSwitchNavigator(
  {
    Login: LoginScreen,
    Home: HomeScreen,
    Product: ProductScreen,
    Cart: CartScreen,
    Checkout: CheckoutScreen,
    Search: SearchScreen,
    ThankYou: ThankYouScreen,
    Orders: OrdersScreen,
    Profile: ProfileScreen
  },
  {

    // The previous screen will slide to the bottom while the next screen will fade in
    transition: (
      <Transition.Together>
        <Transition.Out
          type="slide-bottom"
          durationMs={400}
          interpolation="easeIn"
        />
        <Transition.In type="fade" durationMs={500} />
      </Transition.Together>
    ),
  }
);
*/

//const App = createAppContainer(AppContainer);
